import axios from 'axios'

// axios实例
const service = axios.create({
    // baseURL: process.env.VUE_APP_BASE_API,
    // withCredentials: true,
    timeout: 60 * 1000
})

// request interceptor
service.interceptors.request.use(
    config => {
        return config
    },
    error => {
        console.log(error)
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    response => {
        let res = response.data
        if (res.code === 200) {
            res = res.data
        }
        return res;
    },
    error => {
        console.log('err' + error)
        return Promise.reject(error)
    }
)

export default service
