import request from '@/utils/request'
// 获取地图中心点
export function simpleProject (projectCode) {
    return request({
        url: `https://smart.natappvip.cc/smartenv-system/project/simpleProject/${projectCode}`,
        method: 'get'
    })
}
// 根据租户ID查询所有垃圾桶信息
export function location (projectCode) {
    return request({
        url: `https://smart.natappvip.cc/smartenv-facility/ashcaninfo/location/${projectCode}`,
        method: 'get'
    })
}
// 根据租户ID查询所有中转站信息
export function stations (projectCode) {
    return request({
        url: `https://smart.natappvip.cc/smartenv-facility/facilityinfo/all/${projectCode}`,
        method: 'get'
    })
}
// 根据租户ID查询所有中转站信息
export function facilityAshcanTrack (param) {
    return request({
        url: `https://smart.natappvip.cc/smartenv-facility/facilityinfo/facilityAshcanTrack`,
        method: 'get',
        params: param
    })
}
