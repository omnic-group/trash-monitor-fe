#!/usr/bin/env bash

./stop.sh
docker rmi -f trash-monitor-fe

cd ../
#npm install
npm run build

rm -rf ./docker/dist
mv ./dist ./docker
cd ./docker
cp nginx_prod.conf nginx.conf

docker build -t trash-monitor-fe ./
